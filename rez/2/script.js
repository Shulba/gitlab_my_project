//  Player section
let levelInterval;
let gameEngineRun;
let mapBulletBorderLimitLeft = -1400;
let mapBulletBorderLimitRight = 3400;

class CreatePlayer{
    constructor(pic, life, damage, armore, width, height){
        this.picture = pic;
        this.life = life;
        this.damage = damage;
        this.armore = armore;
        this.width = width;
        this.height = height;
    }
    displayShip(){
        return `<div id="main-ship" class="player-ship"></div>`
    }
}
//  Background World Map -----------------
class GameScene{
    constructor(sceneBackground, level){
        this.sceneBackground = sceneBackground;
        this.level = level;
    }
    createUi(){
        return `<div>${this.level}</div> `
    }
    createMap(){
        return `<div id="main-screen-inner" style="background-image:url('./img/${this.sceneBackground}');" class="screen-map " ></div>`
    }
    changeLevel(){
        this.level += 1;
        if(this.level == 10){
            clearInterval(levelInterval);
        }
        this.sceneBackground = allPictures.allScreen.allScene[this.level-1];
        return `background-image:url('./img/${this.sceneBackground}');`
    }
}
// bullets -----------------
class BulletsDepository{
    constructor(){
        this.allBulletsArr = [];
        this.bulletsId = 0;
    }
    increaseBulletId(){
        this.bulletsId += 1;
        return this.bulletsId
    }
    clearBullets(leftLimit, rightLimit, detectBullet, currentHitBullet){
        let bulletEresseDetect = false;
        if((this.allBulletsArr.length>0)&&(detectBullet == false)){
            for(let i of this.allBulletsArr){
                removeBullet.call(this, i);
                if(bulletEresseDetect){
                    break;
                }
            }
        }else if(detectBullet){
            removeBullet.call(this, currentHitBullet)
        }
        function removeBullet(i){
            let currentBullet = $(`#bullet${i.bulletId}`)
            if(((currentBullet)&&(currentBullet.offset().left >= rightLimit))||((currentBullet)&&(currentBullet.offset().left <= leftLimit))){
                currentBullet.remove();
                this.allBulletsArr.splice(this.allBulletsArr.indexOf(i), 1);
                bulletEresseDetect = true;
            }
        }
    }
}
class BulletsBuild{
    constructor(posX, posY, id, style, owner){
        this.defaultPositionX = posX;
        this.defaultPositionY = posY;
        this.bulletId = id;
        this.damage = 1;
        this.style = style;
        this.owner = owner;
        this.width = 50;
        this.height = 5;
    }
    bulletsInitEvent(displayScreen, allBullets){
        allBullets.increaseBulletId()
        let singleBullet = $("<div></div>")
        .attr(`class`, this.style)
        .attr(`id`, `bullet${this.bulletId}`)
        .css(`margin-top`, this.defaultPositionY)
        .css(`margin-left`, this.defaultPositionX)
        displayScreen.append(singleBullet[0])
        return singleBullet
    }
}
//  Enemy -----------------
class EnemyObjects{
    constructor(){
        this.enemyArr = [];
        this.enemyId = 0;
    }
    idIncrease(){
        this.enemyId += 1;
        return this.enemyId
    }
}
class CreateEnemy{
    constructor(enemyTextures, enemyLife, cssClass, id, width, height){
        this.enemyTextures = enemyTextures;
        this.enemyLife = enemyLife;
        this.cssClass = cssClass;
        this.id = id;
        this.positionsY = 0;
        this.width = width;
        this.height = height;
    }
    placeEnemy(displayScreen, allEnemyArr, playerX, playerY){
        let directionY = Math.floor(Math.random()*600+1);
        let directionX = Math.floor(Math.random()*500+1366)
        let enemyShip = $("<div></div>")
        .attr(`class`, this.cssClass)
        .attr(`id`, `enemyShip_${this.id}`)
        .css(`margin-top`, directionY)
        .css(`margin-left`, directionX)
        .css(`background-image`, `url("./img/${this.enemyTextures}")`)
        displayScreen.append(enemyShip[0])
        this.positionsY = directionY;
    }
}
$(document).ready(function(){
    const mainScreen = $("#main-screen");
    const mainInterface = $("#main-interface");
    let playerXpos, playerYpos = 0;
    let player = new CreatePlayer(allPictures.myShip.textures[0], 3, 5, 4, 50, 5);
    let mainScene = new GameScene(allPictures.allScreen.allScene[0], 1);
    let allEnemy = new EnemyObjects();
    let allBullets = new BulletsDepository();           // all bullets container

    mainScreen.html(mainScene.createMap());
    const mainScreenInner = document.querySelector("#main-screen-inner");   // inner screen with ship bullets
    mainScreenInner.innerHTML = player.displayShip();
    let mainShip = document.querySelector("#main-ship");
    $("#main-screen").on("mousemove", function(event){
        playerXpos = event.originalEvent.clientX-20;
        playerYpos = event.originalEvent.clientY-20;
        mainShip.style = `margin-top:${playerYpos}px; margin-left:${playerXpos}px; background-image: url("./img/${player.picture}")`;
    });
    $("#main-screen").on("click", function(event){
        shootProcess(event.originalEvent.clientX, event.originalEvent.clientY, `player-bullet`, `player`)
    })
    gameStart()
    function gameStart(){
        gameEngineRun = setInterval(function(){
            allBullets.clearBullets(mapBulletBorderLimitLeft, mapBulletBorderLimitRight, false, null);
            enemyProcessing(Math.floor(Math.random()*allPictures.levelEnemy.enemyRandom[mainScene.level-1]+1));
            enemyMoveProcess();
            hitDetection();
        }, 20);
    }
    mainInterface.html(mainScene.createUi());
    function levelChange(){
        //levelInterval = setInterval(function(){
            changeLevelProcess();
       // }, 3000);
    }
    function changeLevelProcess(){
        //mainScene.changeLevel();
        mainScreenInner.style = mainScene.changeLevel();
        mainInterface.html(mainScene.createUi());
    }
    function enemyMoveProcess(){
        if(allEnemy.enemyArr.length>0){
            let currentEnemyShip;
            allEnemy.enemyArr.forEach((allEnemyShip) => {
                let bulletRandom= Math.floor(Math.random()*800+1);
                currentEnemyShip = $(`#enemyShip_${allEnemyShip.id}`)
                if(currentEnemyShip.offset().left<Math.floor(Math.random()*400+800)){
                    if(allEnemyShip.positionsY>playerYpos){
                        allEnemyShip.positionsY-=2;
                    }else if(allEnemyShip.positionsY<playerYpos){
                        allEnemyShip.positionsY+=2;
                    }
                    currentEnemyShip.css(`margin-top`, `${allEnemyShip.positionsY}px`);
                    if(currentEnemyShip.offset().left < -200){
                        /*currentEnemyShip.remove();
                        allEnemy.enemyArr.splice(allEnemy.enemyArr.indexOf(allEnemyShip), 1)*/
                        enemyDelate(currentEnemyShip, allEnemyShip)
                    }
                }
                if((bulletRandom<100)&&(bulletRandom>90)){
                    shootProcess(currentEnemyShip.offset().left-50, currentEnemyShip.offset().top+30, `enemy-bullet`, `enemy`);
                }
            });
        }
    }
    function enemyDelate(currentEnemyShip, allEnemyShip){
        currentEnemyShip.remove();
        allEnemy.enemyArr.splice(allEnemy.enemyArr.indexOf(allEnemyShip), 1)
    }
    function hitDetection(){            // тут будуть пошкодження наші і кораблів відстежуватись
        allEnemy.enemyArr.forEach((allEnemyShip) => {
            let currentShip = $(`#enemyShip_${allEnemyShip.id}`);
            let playerShip = $("#main-ship");
            if(shipBulletsDetect(playerShip, currentShip, allEnemyShip, player)){
                console.log(1)
                enemyDelate(currentShip, allEnemyShip)
                return
            }
            allBullets.allBulletsArr.forEach((allBullet) => {
                let currentBullet = $(`#bullet${allBullet.bulletId}`);
                if(allBullet.owner == `player`){
                    if(shipBulletsDetect(currentShip, currentBullet, allBullet, allEnemyShip)){
                        allBullets.clearBullets(mapBulletBorderLimitLeft, currentShip.offset().left-100, true, allBullet);
                        enemyDelate(currentShip, allEnemyShip)
                        return
                    }
                }else if(allBullet.owner == `enemy`){
                    if( shipBulletsDetect(playerShip, currentBullet, allBullet, allEnemyShip)){
                        allBullets.clearBullets(playerShip.offset().left+100, mapBulletBorderLimitRight, true, allBullet);
                       // alert('hit player')
                        return
                    }
                }
            })
        })
    }
    function shipBulletsDetect(currentShip, currentBullet, allBulletArr, allEnemyShipArr){
        let shipLeft = currentShip.offset().left;
        let shipTop = currentShip.offset().top;
        let bulletLeft = currentBullet.offset().left;
        let bulletTop = currentBullet.offset().top;
        
        if(((shipLeft+allEnemyShipArr.width)>(bulletLeft))&&(shipLeft<(bulletLeft+allBulletArr.width))&&((shipTop+allEnemyShipArr.height)>bulletTop)&&(shipTop<(bulletTop+allBulletArr.height))){ 
            return true
        }else{
            return false
        }
    }
    function shootProcess(bulletPosX, bulletPosY, bulletStyleClass, owner){
        //alert(owner)
        let bullet = new BulletsBuild(bulletPosX, bulletPosY, allBullets.bulletsId, bulletStyleClass, owner);
        bullet.bulletsInitEvent(mainScreenInner, allBullets);
        allBullets.increaseBulletId();
        allBullets.allBulletsArr.push(bullet);
    }
    function enemyProcessing(enemyNumber){
        let enemyType = allPictures.enemy.enemy1;
        let enemyProbability =  Math.floor(Math.random()*allPictures.levelEnemy.enemyInterval[mainScene.level-1]+1);
        if((enemyProbability<100)&&(enemyProbability>95)){
            let i = 0;
            while(i < enemyNumber){
                i++;
                let enemy = new CreateEnemy(allPictures.enemy.enemy1.textures, 1, allPictures.enemy.enemy1.cssClass, allEnemy.idIncrease(), enemyType.shipWidth, enemyType.shipHeight);
                allEnemy.enemyArr.push(enemy);
                enemy.placeEnemy(mainScreenInner, allEnemy, playerXpos, playerYpos);
            }
        }
    }
});
/*
V--map
--level change
V--player
--life
V--enemy
V--delate enemy, bullets
--enemy variation
--boss
V--bullets
--destruction (fire)
V--hit detection
--restart game
*/