//  Player section
let levelInterval;
let gameEngineRun;
let mapBulletBorderLimitLeft = -1400;
let mapBulletBorderLimitRight = 3400;
class CreatePlayer{
    constructor(pic, life, damage, armore, width, height){
        this.picture = pic;
        this.life = life;
        this.damage = damage;
        this.armore = armore;
        this.width = width;
        this.height = height;
    }
    displayShip(){
        return `<div id="main-ship" class="player-ship"><div class="click-area"></div></div>`
    }
    playerLife(reloadAllItems, mainScene){
        this.life -= 1;
        if(this.life<=0){
            mainScene.gameover = true;
            clearInterval(gameEngineRun);
            setTimeout(function(){
                $("#main-ship").remove();
            }, 50)/**/
            setTimeout(function(){
                reloadAllItems()
            }, 5000)
        }
    }
}
//  Background World Map -----------------
class GameScene{
    constructor(sceneBackground, level, score, bossLevel, lastLevel, player){
        this.sceneBackground = sceneBackground;
        this.level = level;
        this.nextBossLevel = bossLevel;
        this.lastLevel = lastLevel;
        this.score = score;
        this.nextLevelPoints = allPictures.levelPoint.points[ this.level-1 ];
        this.bossPresent = false;
        this.backPhrase = '';
        this.gameStart = false;
        this.gameEnd = false;
        this.playerObj = player;
        this.gameover = false;
        this.otherInfo = allPictures.allScreen;
    }
    infoUi(infoInterface){
        let allLifeBloc = '';
        if(this.gameStart){
            for(let i = 0; i<this.playerObj.life; i++){
                allLifeBloc += allPictures.gameHtmlBlock[1];
            }
        }
        let infoContent = `<div class="main-game-info">${allLifeBloc}Score: ${this.score}/${this.nextLevelPoints} points Level: ${this.level}<span class="timer-field">Time left: 00:00s</span></div>`;
        if((this.score == 0)&&(this.gameStart == false)){
            return `<div id="info-ui-interface" class="info-ui-interface" > ${infoContent} </div> `
        }else if((this.score >= 0)&&(this.gameStart)){
            infoInterface.html(`${infoContent}`)
        }else if(this.gameStart == false){
            return `<div id="info-ui-interface" class="info-ui-interface"> </div> `;
        }
        if((this.gameEnd)||(this.gameover)){
            let phrases = '';
            (this.gameEnd)?phrases = `<p class="small-text-2">${this.otherInfo.gameText.default[4]} You score: ${this.score}</p>`:
            phrases = `<p class="small-text-2">${this.otherInfo.gameText.default[3]} You score: ${this.score}</p>`
            this.gameEnd = false;
            this.gameover = false;
            this.gameStart = false;
            infoContent = `<div class="start-screen-wrapper">
                <div class="logo-wrapper">
                    <img class="main-logo" alt="space Impact" src="./img/${this.otherInfo.gameOtherPictures[0]}">
                    </div>
                    <div class="name-wrapper">
                    ${phrases}
                    </div>
                </div>`;
                infoInterface.html(`${infoContent}`)
                clearInterval(gameEngineRun);
        }
    }
    gameUi(infoInterface){
        let infoContent = '';
       if(this.gameStart == false){
            infoContent = `<div class="start-screen-wrapper">
                <div class="logo-wrapper">
                    <img class="main-logo" alt="space Impact" src="./img/${this.otherInfo.gameOtherPictures[0]}">
                    </div>
                    <div class="name-wrapper"><span class="large-text">${this.otherInfo.gameText.default[0]}</span>
                    <span class="small-text">${this.otherInfo.gameText.default[1]}</span>
                    </div>
                <p><span id="start-button" class="clickable-area">${this.otherInfo.gameText.default[2]}</span></p>
                </div>`;
        }
        infoInterface.html(infoContent)
    }
    gameScoreCounter(points, reloadAllItems, gameEnds){
        this.score += points;
        if((this.score>=this.nextLevelPoints)&&(this.level < this.lastLevel)){
            this.level += 1;
            this.changeLevel()
        }else if((this.score>=this.nextLevelPoints)&&(this.level == this.lastLevel)){
            this.gameEnd = true;
            this.bossPresent = true;
            setTimeout(function(){
                reloadAllItems()
            }, 6000)
        }
    }
    createMap(){
        return `<div id="main-screen-inner" style="background-image:url('./img/${this.sceneBackground}');" class="screen-map " ></div>`
    }
    changeLevel(){
        this.nextLevelPoints = allPictures.levelPoint.points[ this.level-1];
        this.sceneBackground = allPictures.allScreen.allScene[this.level-1];
        this.screen.style =`background-image:url('./img/${this.sceneBackground}');`;
    }
}
// sound----------------
class CreateSound{
    constructor (sourse, soundLoop, volume){
        this.mainSound= new Audio();
        this.mainSound.src=`./audio/${sourse}`;
        this.mainSound.autoplay= true;
        this.mainSound.loop = soundLoop;
        this.mainSound.volume = volume;
    }
    sound_stop(){
        this.mainSound.pause();
		this.mainSound.currentTime = 0.0
    }
    soundPlay(){
        this.mainSound.play();
    }
}
// bullets -----------------
class BulletsDepository{
    constructor(){
        this.allBulletsArr = [];
        this.bulletsId = 0;
    }
    increaseBulletId(){
        this.bulletsId += 1;
    }
    clearBullets(leftLimit, rightLimit, detectBullet, currentHitBullet){
        let bulletEresseDetect = false;
        if((this.allBulletsArr.length>0)&&(detectBullet == false)){
            for(let i of this.allBulletsArr){
                removeBullet.call(this, i);
                if(bulletEresseDetect){
                    break;
                }
            }
        }else if(detectBullet){
            removeBullet.call(this, currentHitBullet)
        }
        function removeBullet(i){
            let currentBullet = $(`#bullet${i.bulletId}`)
            if(((currentBullet)&&(currentBullet.offset().left >= rightLimit))||((currentBullet)&&(currentBullet.offset().left <= leftLimit))){
                currentBullet.remove();
                this.allBulletsArr.splice(this.allBulletsArr.indexOf(i), 1);
                bulletEresseDetect = true;
            }
        }
    }
}
class BulletsBuild{
    constructor(posX, posY, id, style, owner){
        this.defaultPositionX = posX;
        this.defaultPositionY = posY;
        this.bulletId = id;
        this.damage = 1;
        this.style = style;
        this.owner = owner;
        this.width = 50;
        this.height = 5;
    }
    bulletsInitEvent(displayScreen, allBullets){
        let singleBullet = $("<div></div>")
        .attr(`class`, this.style)
        .attr(`id`, `bullet${this.bulletId}`)
        .css(`margin-top`, this.defaultPositionY)
        .css(`margin-left`, this.defaultPositionX)
        displayScreen.append(singleBullet[0])
        return singleBullet
    }
}
//  Enemy -----------------
class EnemyObjects{
    constructor(){
        this.enemyArr = [];
        this.enemyId = 0;
    }
    idIncrease(){
        this.enemyId += 1;
        return this.enemyId
    }
}
class CreateEnemy{
    constructor(enemyInnerStyle, enemyTextures, enemyLife, cssClass, id, width, height, type, value, bossImpact, controlled){
        this.enemyInnerStyle = enemyInnerStyle;
        this.enemyTextures = enemyTextures;
        this.defaultLife = enemyLife;
        this.enemyLife = enemyLife;
        this.cssClass = cssClass;
        this.id = id;
        this.positionsY = 0;
        this.positionsX = 0;
        this.width = width;
        this.height = height;
        this.type = type;
        this.value = value;
        this.domElement = '';
        this.texturePosition = 0;
        this.middleHeight = Math.round(height/2);
        this.bossImpact = bossImpact[0];
        this.bossImpactState = bossImpact[1];/**/
        this.bossImpactWertical = bossImpact[2];
        this.controlled = controlled;
    }
    laserPointDetect(){
        if(this.type == 'enemyBoss'){
            this.middleHeight = Math.floor(Math.random()*this.height+1)
        }
        return this.middleHeight
    }
    placeEnemy(displayScreen, allEnemyArr, playerX, playerY){
        let directionY, directionX;
        if(this.type != 'flame'){
            directionY = Math.floor(Math.random()*600+1);
            directionX = Math.floor(Math.random()*500+1366);
        }else{
            directionY = playerY;
            directionX = playerX;
        }
        let enemyShip = $("<div></div>")
        .attr(`class`, this.cssClass)
        .attr(`id`, `enemyShip_${this.id}`)
        .css(`margin-top`, directionY)
        .css(`margin-left`, directionX)
        displayScreen.append(enemyShip[0])
        let currentObject = $(`#enemyShip_${this.id}`);
        this.domElement = currentObject;
        if(this.type != 'flame'){
            currentObject.html(`<div id="shipInnerBlock${this.id}" class="${this.enemyInnerStyle}"></div>`)
            $(`#shipInnerBlock${this.id}`).css(`background-image`, `url("./img/${this.enemyTextures}")`);
        }
        this.positionsY = directionY;
        this.positionsX = directionX;
        if(this.type == 'flame'){
            let clearFlame;
            clearFlame = setTimeout(function(){
                currentObject.remove();
            }, 2000);
        }
    }
    enemyLifeDamage(mainScene){
        if(mainScene.level!=mainScene.nextBossLevel){
            this.texturePosition -= this.height;
            this.domElement.children().css(`background-position-y`, this.texturePosition);
        }else{
            let bossLifeCoef = this.enemyLife/(Math.round(this.defaultLife / 4))
            if(bossLifeCoef.toString().length ===1){
                this.texturePosition -= this.height;
                this.domElement.children().css(`background-position-y`, this.texturePosition);
            }
        }
        this.enemyLife -= 1;
        if((mainScene.bossPresent==true)&&(this.enemyLife <= 0)&&(this.type == 'enemyBoss')){
            mainScene.bossPresent=false;
            mainScene.nextBossLevel = allPictures.levelEnemy.bossLevels[allPictures.levelEnemy.bossLevels.indexOf(mainScene.nextBossLevel)];
            mainScene.score =  mainScene.nextLevelPoints;
        }
        return this.enemyLife
    }
    moveDirections(playerPositionY, coefY, coefX, mainScene){
        let boossAdjust = 0;
        if(mainScene.bossPresent){
            if(this.positionsX>1100){               // останній бос може ходити вперед і назад
                this.positionsX -=coefX;
            }else if(this.bossImpact){
                let impactRandom = Math.floor(Math.random()*2000+1);
                if((impactRandom>80)&&(impactRandom<95)&&(this.bossImpactState == false)){
                    this.bossImpactState = true;
                    this.richImpact = false;
                }
                if((this.bossImpactState)&&(this.positionsX>10)&&(this.richImpact==false)){
                    this.positionsX -=coefX+5;
                    if(this.positionsX<=10){
                        this.richImpact = true;
                    }
                }else if((this.bossImpactState)&&(this.positionsX<1100)){
                    this.positionsX +=coefX;
                    if(this.positionsX>=1100){
                        this.richImpact = false;
                        this.bossImpactState = false;
                    }
                }
            }/**/
            boossAdjust = this.middleHeight;
        }
        if((this.positionsY>playerPositionY-boossAdjust)&&((this.bossImpactWertical ==false)||(this.bossImpactState==false))){
            this.positionsY-=coefY;
        }else if((this.positionsY<playerPositionY-boossAdjust)&&((this.bossImpactWertical ==false)||(this.bossImpactState==false))){
            this.positionsY+=coefY;
        }
        this.domElement.css(`margin-top`, `${this.positionsY}px`);
        this.domElement.css(`margin-left`, `${this.positionsX}px`);
    }
}
$(document).ready(function(){
    const mainScreen = $("#main-screen");
    const mainInterface = $("#main-interface");
    let playerXpos, playerYpos, player, mainScene, allEnemy, allBullets, infoUiInterface,mainScreenInner, mainShip, startButton, gameSound = 0;
    reloadAllItems()
    function reloadAllItems(){
        clearInterval(gameEngineRun);
        player = new CreatePlayer(allPictures.myShip.textures[0], allPictures.myShip.life, 5, 4, allPictures.myShip.shipWidth, allPictures.myShip.shipHeight);
        mainScene = new GameScene(allPictures.allScreen.allScene[0], 1, 0, allPictures.levelEnemy.bossLevels[0], allPictures.levelEnemy.enemyRandom.length, player);
        allEnemy = new EnemyObjects();
        gameSound = new CreateSound(allPictures.gameSound[0], true, .4);
        allBullets = new BulletsDepository();           // all bullets container
        mainInterface.html(mainScene.infoUi());
        infoUiInterface = $("#info-ui-interface");
        mainScreen.html(mainScene.createMap())
        .prepend($("<div></div>").attr(`class`, `glass`))
        mainScene.gameStart = false;
        mainScene.gameUi(infoUiInterface)
        startButton = $("#start-button");
        startButton.on("click", function(){
            mainScene.gameStart = true;
            mainScene.infoUi(infoUiInterface);
            loadItems()
            gameStart()
        })
    }
   function loadItems(){
        gameSound.sound_stop()
        gameSound = new CreateSound(allPictures.gameSound[1], true, .4);
        mainScreenInner = document.querySelector("#main-screen-inner");   // inner screen with ship bullets
        mainScreenInner.innerHTML = player.displayShip();
        mainScene.screen = mainScreenInner;                                 // in object added dom element
        mainShip = document.querySelector("#main-ship");        // для оновлення гри потрбно переназначити основні об'єкти
    }
    $("#screen-wrapper").on("mousemove", function(event){
            if(mainScene.gameStart){
                playerXpos = event.originalEvent.clientX-20;
                playerYpos = event.originalEvent.clientY-20;
                mainShip.style = `margin-top:${playerYpos}px; margin-left:${playerXpos}px; background-image: url("./img/${player.picture}")`;
            }
        });
        $("#screen-wrapper").on("click", function(event){
            if(mainScene.gameStart){
                new CreateSound(allPictures.gameSound[2], false, .4);
                shootProcess(event.originalEvent.clientX, event.originalEvent.clientY, `player-bullet`, `player`)
            }
        })
    function gameStart(){
        gameEngineRun = setInterval(function(){
            allBullets.clearBullets(mapBulletBorderLimitLeft, mapBulletBorderLimitRight, false, null);
            let enemyRandom =  Math.floor(Math.random()*allPictures.levelEnemy.enemyRandom[mainScene.level-1]+1)
            enemyProcessing(enemyRandom);
            enemyMoveProcess();
            hitDetection();
            mainScene.infoUi(infoUiInterface);
        }, 20);
    }
    function gameEnds(){
        mainScene.infoUi(infoUiInterface);
    }
    function enemyMoveProcess(){
        if(allEnemy.enemyArr.length>0){
            let bulletClasses;
            let currentEnemyShip;
            let enemyShootProbability = allPictures.levelEnemy.enemyShootProbability[mainScene.level-1];
            allEnemy.enemyArr.forEach((allEnemyShip) => {
                let bulletRandom= Math.floor(Math.random()*800+1);
                currentEnemyShip = $(`#enemyShip_${allEnemyShip.id}`)
                if(currentEnemyShip.offset().left<Math.floor(Math.random()*400+800)){
                    allEnemyShip.moveDirections(playerYpos, 2, null, mainScene)
                }
                if((mainScene.bossPresent == true)&&(allEnemyShip.type == 'enemyBoss')){
                        allEnemyShip.moveDirections(playerYpos, 1, 2, mainScene);
                        if(mainScene.level>=10){
                            let bulletChoise = Math.floor(Math.random()*10+1);
                            if(bulletChoise<=2){
                                bulletClasses = `enemy-bullet-top`
                            }else if(bulletChoise<=4){
                                bulletClasses = `enemy-bullet-bottom`
                            }else{
                                 bulletClasses = `enemy-bullet`
                            }
                        }else{
                            bulletClasses = `enemy-bullet`
                        }
                }else{bulletClasses = `enemy-bullet`}
                if(currentEnemyShip.offset().left < -200){
                    enemyDelate(currentEnemyShip, allEnemyShip)
                }
                if((bulletRandom<enemyShootProbability[0])&&(bulletRandom>enemyShootProbability[1])){
                    new CreateSound(allPictures.gameSound[5], false, .2);
                    shootProcess(currentEnemyShip.offset().left-50, currentEnemyShip.offset().top+allEnemyShip.laserPointDetect(), bulletClasses, `enemy`);
                }
            });
        }
    }
    function enemyDelate(currentEnemyShip, allEnemyShip){
        currentEnemyShip.remove();
        allEnemy.enemyArr.splice(allEnemy.enemyArr.indexOf(allEnemyShip), 1)
    }
    function flame(shipOfFlame, leftCorrection, topCorrection){
        new CreateSound(allPictures.gameSound[3], false, .4);
        let enemy = new CreateEnemy(null, null, 1, 'flame_block', allEnemy.idIncrease(), 0, 0, 'flame', 5, [false, false], false);
        enemy.placeEnemy(mainScreenInner, allEnemy, shipOfFlame.offset().left-leftCorrection, shipOfFlame.offset().top-topCorrection);
    }
    let hitTimes = false;
    function hitDetection(){            // тут будуть пошкодження наші і кораблів відстежуватись
        allEnemy.enemyArr.forEach((allEnemyShip) => {
            let currentShip = $(`#enemyShip_${allEnemyShip.id}`);
            let playerShip = $("#main-ship");
            if((shipBulletsDetect(playerShip, currentShip, allEnemyShip, player))&&hitTimes==false){           // Destroy enemy ship collision
                allEnemyShip.enemyLifeDamage(mainScene)
                flame(currentShip, 0, 0);
                player.playerLife(reloadAllItems, mainScene);
                hitTimes = true;
                setTimeout(function(){hitTimes = false}, 1000)
                if(allEnemyShip.enemyLife == 0){
                    new CreateSound(allPictures.gameSound[4], false, .4);
                    enemyDelate(currentShip, allEnemyShip)
                    mainScene.gameScoreCounter(10, reloadAllItems, gameEnds);
                }return
            }
            allBullets.allBulletsArr.forEach((allBullet) => {
                let currentBullet = $(`#bullet${allBullet.bulletId}`);
                if(allBullet.owner == `player`){                                            // Destroy enemy ship / player bullet
                    if(shipBulletsDetect(currentShip, currentBullet, allBullet, allEnemyShip)){
                        allEnemyShip.enemyLifeDamage(mainScene)
                        flame(currentBullet, 10, 50);
                        allBullets.clearBullets(mapBulletBorderLimitLeft, currentShip.offset().left-100, true, allBullet);
                        if(allEnemyShip.enemyLife == 0){
                            new CreateSound(allPictures.gameSound[4], false, .4);
                            enemyDelate(currentShip, allEnemyShip)
                            mainScene.gameScoreCounter(50, reloadAllItems, gameEnds);
                        }
                        return
                    }
                }else if(allBullet.owner == `enemy`){                                       // enemy bullets hit detection at player
                    if( shipBulletsDetect(playerShip, currentBullet, allBullet, player)){
                        if(mainScene.bossPresent == true){
                        }
                        new CreateSound(allPictures.gameSound[4], false, .4);
                        player.playerLife(reloadAllItems, mainScene)
                        flame(currentBullet, 50, 30);
                        allBullets.clearBullets(playerShip.offset().left+100, mapBulletBorderLimitRight, true, allBullet);
                        return
                    }
                }
            })
        })
    }
    function shipBulletsDetect(currentShip, currentBullet, allBulletArr, allEnemyShipArr){          // collision detect true/false
        let shipLeft, shipTop, bulletLeft, bulletTop = 0;
        if((currentShip.offset().left)&&(currentShip.offset().top)&&(currentBullet.offset().left)&&(currentBullet.offset().top)){
            shipLeft = currentShip.offset().left;
            shipTop = currentShip.offset().top;
            bulletLeft = currentBullet.offset().left;   //
            bulletTop = currentBullet.offset().top;
        }
        if(((shipLeft+allEnemyShipArr.width)>bulletLeft)&&(shipLeft<(bulletLeft+allBulletArr.width))&&((shipTop+allEnemyShipArr.height)>bulletTop)&&(shipTop<(bulletTop+allBulletArr.height))){
            return true
        }else{
            return false
        }
    }
    function shootProcess(bulletPosX, bulletPosY, bulletStyleClass, owner){                         // create bullets objects  // ще має бути спрорахування стрільбу в бік
        let bullet = new BulletsBuild(bulletPosX, bulletPosY, allBullets.bulletsId, bulletStyleClass, owner);  // для босів пулі маю порізному діяти
        bullet.bulletsInitEvent(mainScreenInner, allBullets);
        allBullets.increaseBulletId();
        allBullets.allBulletsArr.push(bullet);
    }
    function enemyProcessing(enemyNumber){
        let levelEnemyObj = allPictures.levelEnemy;
        let enemyObject = allPictures.enemy;
        let enemyProbability =  Math.floor(Math.random()*levelEnemyObj.enemyInterval[mainScene.level-1]+1);
        let currentEnemy = enemyTypeDefind(enemyObject);
        if(mainScene.level == mainScene.nextBossLevel){
            if(mainScene.bossPresent== false){
                let bossArrElement = 0;
                (mainScene.level==5)? bossArrElement = 0 :(mainScene.level==10)? bossArrElement = 1 : ``;
                setTimeout(function(){
                    bossArrElement = enemyObject.bossEnemy[bossArrElement]
                    createNewEnemy(bossArrElement.innerCssClass, bossArrElement.textures, bossArrElement.life, bossArrElement.cssClass, allEnemy.idIncrease(), bossArrElement.shipWidth, bossArrElement.shipHeight, 'enemyBoss', bossArrElement.enemyImpact, bossArrElement.controlled);
                }, 5000)
                mainScene.bossPresent = true;
            }
        }
        if((enemyProbability<levelEnemyObj.enemySpawnProbability[mainScene.level-1][0])&&(enemyProbability>levelEnemyObj.enemySpawnProbability[mainScene.level-1][1])&&(mainScene.bossPresent==false)){
            let i = 0;
            while(i < enemyNumber){
                i++;
                if(mainScene.bossPresent==false){
                    currentEnemy = enemyTypeDefind(enemyObject);
                    if(currentEnemy){
                        createNewEnemy(currentEnemy.innerCssClass, currentEnemy.textures, currentEnemy.life, currentEnemy.cssClass, allEnemy.idIncrease(), currentEnemy.shipWidth, currentEnemy.shipHeight, 'enemyShip', currentEnemy.enemyImpact, currentEnemy.controlled);
                    }
                }
            }
        }
        function createNewEnemy(innerCss,textures, life, styleClass, id, width, height, status, impact, controlled){
            let enemy = new CreateEnemy(innerCss, textures, life, styleClass, id, width, height, status, 5, impact, controlled);
            allEnemy.enemyArr.push(enemy);
            enemy.placeEnemy(mainScreenInner, allEnemy, playerXpos, playerYpos);
        }
    }
    function enemyTypeDefind(enemyObject){
        let res = enemyObject.regularEnemy[0];
        if(mainScene.bossPresent == true){
            if(mainScene.level == 5){
                res = enemyObject.bossEnemy[0];
            }else if(mainScene.level == 10){
                res = enemyObject.bossEnemy[1];
            }
        }else if(mainScene.level <2){
            res = enemyObject.regularEnemy[0];
        }else{
            res = enemyObject.regularEnemy[Math.floor(Math.random()*mainScene.level)];
        }
        return res
    }
    $(document).on("keydown", function(event){
        let currentCommandIndex = 0;
        let currentCommand = allPictures.keys[0].find((x) => {  currentCommandIndex = allPictures.keys[0].indexOf(x)+1; if(x === event.originalEvent.keyCode){ return x}})
        if(currentCommand){
            if(currentCommandIndex>10){
                if(currentCommandIndex===11){ currentCommandIndex = `L` }else if(currentCommandIndex===12){  currentCommandIndex = `esc`}else{ mainScene.backPhrase=``, currentCommandIndex= ``};
            }
            mainScene.backPhrase += currentCommandIndex
            if((mainScene.backPhrase == `L${((currentCommandIndex>0)&&(currentCommandIndex<=9))? currentCommandIndex: false}`)){                 // V
               mainScene.bossPresent = false;
                mainScene.score = allPictures.levelPoint.points[currentCommandIndex-1] - 1000;    // V
                mainScene.level = currentCommandIndex;
                mainScene.changeLevel();
                mainScene.backPhrase = ``;
                if(mainScene.level<5){
                    mainScene.nextBossLevel = 5;
                }else if(mainScene.level>=5){
                    mainScene.nextBossLevel = 10;
                }
            }
            else if(event.originalEvent.keyCode === 27){
                if(mainScene.gameStart == true){
                    let myAlart = $("<div></div>")
                    .attr(`id`, `mainAlert`)
                    .attr(`class`, `main-alert`)
                    mainInterface.append(myAlart);
                    mainScene.gameStart = false;
                    clearInterval(gameEngineRun);
                    $("#mainAlert").html(allPictures.gameHtmlBlock[0])
                    $("#mainAlert").children().on('click', 'button', function(){
                        if(this.value==1){
                            gameSound.sound_stop()
                            reloadAllItems();
                        }else{
                            gameStart(), myAlart.remove();
                            mainScene.gameStart = true
                        }
                    })
                }
                //
            }
        }
    }) /**/
});
/*
V--map
V-level change
V--player
V--life
--asteroids, more enemy
--new levels
--resuplyLife
V--sound
V--enemy
V--delate enemy, bullets
V-enemy variation
V-boss
V--boss impact
V--boss randome Shot
V--bullets
V-destruction (fire)
V--hit detection
V-restart game
V-UI
V-cheats

49  /1
50  /2
51  /3
52  /4
53  /5
54  /6
55  /7
56  /8
57  /9
48  /10
76  /L

27 / esc
8  / Backspace

311 < 827 max
*/