//  Player section
let levelInterval;
let gameEngineRun;
class CreatePlayer{
    constructor(pic, life, damage, armore){
        this.picture = pic;
        this.life = life;
        this.damage = damage;
        this.armore = armore;
    }
    displayShip(){
        return `<div id="main-ship" class="player-ship"></div>`
    }
}
//  Background World Map -----------------
class GameScene{
    constructor(sceneBackground, level){
        this.sceneBackground = sceneBackground;
        this.level = level;
    }
    createUi(){
        return `<div>${this.level}</div> `
    }
    createMap(){
        return `<div id="main-screen-inner" style="background-image:url('./img/${this.sceneBackground}');" class="screen-map " ></div>`
    }
    changeLevel(){
        this.level += 1;
        if(this.level == 10){
            clearInterval(levelInterval);
        }
        this.sceneBackground = allPictures.allScreen.allScene[this.level-1];
        return `background-image:url('./img/${this.sceneBackground}');`
    }
}
// bullets -----------------
class BulletsDepository{
    constructor(){
        this.allBulletsArr = [];
        this.bulletsId = 0;
    }
    increaseBulletId(){
        this.bulletsId += 1;
        return this.bulletsId
    }
}
class BulletsBuild{             // треба перебудувати по образу кораблів щоб в конструкторі задавати чия це куля ворожа чи своя/ тожді можна відстежити пошкодження
    constructor(){
        this.allBulletsArr = [];
        this.bulletsId = 0;
    }
    bulletsInitEvent(displayScreen, directionX, directionY, bulletClass){
        this.allBulletsArr.push(this.increaseBulletId());
        let singleBullet = $("<div></div>")
        .attr(`class`, bulletClass)
        .attr(`id`, `${this.bulletsId}`)
        .css(`margin-top`, directionY)
        .css(`margin-left`, directionX)
        displayScreen.append(singleBullet[0])
    }
    increaseBulletId(){
        this.bulletsId += 1;
        return this.bulletsId
    }
    clearBullets(displayScreen){
        let bulletEresseDetect = false;
        if(this.allBulletsArr.length>0){
            for(let i of this.allBulletsArr){
                removeBullet.call(this, i);
                if(bulletEresseDetect){
                    break;
                }
            }
        }
        function removeBullet(i){
            if((($(`#${i}`))&&($(`#${i}`).offset().left >= 3400))||(($(`#${i}`))&&($(`#${i}`).offset().left <= -1400))){
                $(`#${i}`).remove();
                this.allBulletsArr.splice(this.allBulletsArr.indexOf(i), 1);
                bulletEresseDetect = true;
            }
        }
    }
}
//  Enemy -----------------
class EnemyObjects{
    constructor(){
        this.enemyArr = [];
        this.enemyId = 0;
    }
    idIncrease(){
        this.enemyId += 1;
        return this.enemyId
    }
}
class CreateEnemy{
    constructor(enemyTextures, enemyLife, cssClass, id){
        this.enemyTextures = enemyTextures;
        this.enemyLife = enemyLife;
        this.cssClass = cssClass;
        this.id = id;
        this.positionsY = 0;
    }
    placeEnemy(displayScreen, allEnemyArr, playerX, playerY){
        let directionY = Math.floor(Math.random()*600+1);
        let directionX = Math.floor(Math.random()*500+1366)
        //alert(this.id)
        let enemyShip = $("<div></div>")
        .attr(`class`, this.cssClass)
        .attr(`id`, `enemyShip_${this.id}`)
        .css(`margin-top`, directionY)
        .css(`margin-left`, directionX)
        .css(`background-image`, `url("./img/${this.enemyTextures}")`)

        displayScreen.append(enemyShip[0])
        this.positionsY = directionY;
    }
}
$(document).ready(function(){
    const mainScreen = $("#main-screen");
    const mainInterface = $("#main-interface");
    let playerXpos, playerYpos = 0;
    let bulletsInit = new BulletsBuild();
    let player = new CreatePlayer(allPictures.myShip.textures[0], 3, 5, 4);
    let mainScene = new GameScene(allPictures.allScreen.allScene[0], 1);
    let allEnemy = new EnemyObjects();

    mainScreen.html(mainScene.createMap());
    const mainScreenInner = document.querySelector("#main-screen-inner");   // inner screen with ship bullets
    mainScreenInner.innerHTML = player.displayShip();
    let mainShip = document.querySelector("#main-ship");

    $("#main-screen").on("mousemove", function(event){
        playerXpos = event.originalEvent.clientX-20;
        playerYpos = event.originalEvent.clientY-20;
        mainShip.style = `margin-top:${playerYpos}px; margin-left:${playerXpos}px; background-image: url("./img/${player.picture}")`;
    });
    $("#main-screen").on("click", function(event){
        bulletsInit.bulletsInitEvent(mainScreenInner, event.originalEvent.clientX, event.originalEvent.clientY, `player-bullet`);     //mainScreenInner
    })

    gameStart()
    function gameStart(){
        gameEngineRun = setInterval(function(){
            bulletsInit.clearBullets(mainScreenInner);
            enemyProcessing(Math.floor(Math.random()*allPictures.levelsEnemy.enemyRandom[mainScene.level-1]+1));
            enemyMoveProcess();
            hitDetection();
        }, 20);
    }
    mainInterface.html(mainScene.createUi());
    function levelChange(){
        //levelInterval = setInterval(function(){
            changeLevelProcess();
       // }, 3000);
    }
    function changeLevelProcess(){
        //mainScene.changeLevel();
        mainScreenInner.style = mainScene.changeLevel();
        mainInterface.html(mainScene.createUi());
    }
    function enemyMoveProcess(){
        if(allEnemy.enemyArr.length>0){
            let currentEnemyShip;
            allEnemy.enemyArr.forEach((allEnemyShip) => {
                let bulletRandom= Math.floor(Math.random()*800+1);
                currentEnemyShip = $(`#enemyShip_${allEnemyShip.id}`)
                if(currentEnemyShip.offset().left<Math.floor(Math.random()*400+800)){
                    if(allEnemyShip.positionsY>playerYpos){
                        allEnemyShip.positionsY-=2;
                    }else if(allEnemyShip.positionsY<playerYpos){
                        allEnemyShip.positionsY+=2;
                    }
                    currentEnemyShip.css(`margin-top`, `${allEnemyShip.positionsY}px`);
                    if(currentEnemyShip.offset().left < -200){
                        currentEnemyShip.remove();
                        allEnemy.enemyArr.splice(allEnemy.enemyArr.indexOf(allEnemyShip), 1)
                    }
                }
                if((bulletRandom<100)&&(bulletRandom>90)){
                    bulletsInit.bulletsInitEvent(mainScreenInner, currentEnemyShip.offset().left, currentEnemyShip.offset().top, `enemy-bullet`);
                }
            });
        }
    }
    function hitDetection(){            // тут будуть пошкодження наші і кораблів відстежуватись
        allEnemy.enemyArr.forEach((allEnemyShip) => {
            let currentShip = $(`#enemyShip_${allEnemyShip.id}`);
            let currentBullets = bulletsInit.allBulletsArr;
        })
        

       // console.log(bulletsInit.allBulletsArr);
        //alert(1)

    }
    function enemyProcessing(enemyNumber){
        let enemyProbability =  Math.floor(Math.random()*allPictures.levelsEnemy.enemyInterval[mainScene.level-1]+1);
        if((enemyProbability<100)&&(enemyProbability>95)){
            let i = 0;
            while(i < enemyNumber){
                i++;
                let enemy = new CreateEnemy(allPictures.enemy.enemy1.textures, 1, allPictures.enemy.enemy1.cssClass, allEnemy.idIncrease())
                allEnemy.enemyArr.push(enemy);
                enemy.placeEnemy(mainScreenInner, allEnemy, playerXpos, playerYpos);
            }
        }
    }
});
/*
V--map
--level change
V--player
--life
V--enemy
V--delate enemy, bullets
--enemy variation
--boss
V--bullets
--destruction (fire)
--hit detection
*/