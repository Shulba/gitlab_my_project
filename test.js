
let obj = {
    firstName: "Johny",
    lastName: "Cash",
    fullName(){
        return `${this.firstName} ${this.lastName}`
    }
}

let v1 = JSON.stringify(obj)
let v2 = JSON.parse(v1)

console.log(obj.fullName())
console.log(v1)
console.log(v2)

